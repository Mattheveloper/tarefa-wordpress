<footer>
        <div class="gradient"></div>
        <section class="box">
            <div class="insideBox">
                <iframe class="iframe"
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.190168757374!2d-43.13558574951957!3d-22.906355584938527!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ed79f10f3%3A0xb39c7c0639fbc9e8!2sIN%20Junior%20-%20Empresa%20Junior%20de%20Computa%C3%A7%C3%A3o%20da%20UFF!5e0!3m2!1spt-BR!2sbr!4v1659372100059!5m2!1spt-BR!2sbr"
                    width="250" height="150" style="border:0;" allowfullscreen="" loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade">
                </iframe>
                <section class="contato">
                    <div>
                        <i class="material-icons">location_on</i>
                        <p>Av. Milton Tavares de Souza, s/n - Sala 115 B - <br>Boa Viagem, Niterói - RJ, 24210-315</p>
                    </div>
                    <div>
                        <i class="material-icons">call</i>
                        <p>(99) 99999-9999</p>
                    </div>
                    <div>
                        <i class="material-icons">email</i>
                        <p>salve-lobos@lobINhos.com</p>
                    </div>
                    <div class="contact">
                        <a href="../quemSomos/quemSomos.html">Quem Somos</a>
                    </div>
                </section>
            </div>

            <div class="paws">
                <p>Desenvolvido com</p>
                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/paws.svg" alt="">
            </div>
        </section>
    </footer>
    <?php wp_footer(); ?>
</body>

</html>